const fName = document.getElementById("fName");
const lName = document.getElementById("lName");
const submitButton = document.querySelector("#submit-button");
const totalScore = document.querySelector("#total-score");
const correctAnswers = {
      "question-1": "wolf",
      "question-2": "boy"
    };

submitButton.addEventListener("click", () => {
  let score = 0;
  
  for (const [questionId, answer] of Object.entries(correctAnswers)) {
    const selectedAnswer = document.querySelector(`#${questionId}`).value;
    const messageElement = document.querySelector(`#${questionId}-message`);

    if (selectedAnswer === answer) {
      messageElement.innerText = "Correct!";
      messageElement.style.color = "green";
      score++;
    } else {
      messageElement.innerText = "Incorrect!?";
      messageElement.style.color = "red";
    }
  }

  totalScore.innerText = `Good day, ${fName.value} ${lName.value}. You have ${score}/${Object.keys(correctAnswers).length} question correct.`;

  alert(totalScore.innerText);
});